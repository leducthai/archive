package archive

import (
	"time"

	"gitlab.com/leducthai/archive/utils/roles"
)

type LoginRequest struct {
	Username string
	Password string
}

type LoginReply struct {
	Token           string
	TokenExpiryTime time.Time
	Role            []roles.Role
}
