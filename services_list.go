package archive

// DataManagerServices define services in database
type DataManagerServices interface {
	Profile() ProfileServices
	Authentication() AuthenticationService
}
