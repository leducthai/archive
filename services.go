package archive

import "context"

// ProfileServices define the profile service
type ProfileServices interface {
	GetProfile(context.Context, GetProfileRequest) (GetProfileReply, error)
}

// AuthenticationService definition.
type AuthenticationService interface {
	Login(context.Context, LoginRequest) (LoginReply, error)
}
