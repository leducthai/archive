// Code generated by protoc-gen-go. DO NOT EDIT.
// source: code.proto

package errors

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Must advise developer of any change.
type Code int32

const (
	Code_NONE                              Code = 0
	Code_ACCOUNT_NOT_FOUND_OR_BAD_PASSWORD Code = 10000
	Code_USER_NO_PERMISSION                Code = 10100
	Code_USER_UNKNOWN_TOKEN                Code = 10300
	Code_USER_ALREADY_EXISTS               Code = 10400
	Code_ACCOUNT_ROLES_NOT_PERMIT          Code = 10500
	Code_USER_NOT_FOUND                    Code = 10800
	Code_ACCOUNT_ALREADY_EXISTS            Code = 11000
	Code_ACCOUNT_NOT_FOUND                 Code = 12000
	// 13xxx for user sign in/out errors
	Code_PREVIOUS_USER_NOT_SIGNED_OUT Code = 13000
)

var Code_name = map[int32]string{
	0:     "NONE",
	10000: "ACCOUNT_NOT_FOUND_OR_BAD_PASSWORD",
	10100: "USER_NO_PERMISSION",
	10300: "USER_UNKNOWN_TOKEN",
	10400: "USER_ALREADY_EXISTS",
	10500: "ACCOUNT_ROLES_NOT_PERMIT",
	10800: "USER_NOT_FOUND",
	11000: "ACCOUNT_ALREADY_EXISTS",
	12000: "ACCOUNT_NOT_FOUND",
	13000: "PREVIOUS_USER_NOT_SIGNED_OUT",
}

var Code_value = map[string]int32{
	"NONE":                              0,
	"ACCOUNT_NOT_FOUND_OR_BAD_PASSWORD": 10000,
	"USER_NO_PERMISSION":                10100,
	"USER_UNKNOWN_TOKEN":                10300,
	"USER_ALREADY_EXISTS":               10400,
	"ACCOUNT_ROLES_NOT_PERMIT":          10500,
	"USER_NOT_FOUND":                    10800,
	"ACCOUNT_ALREADY_EXISTS":            11000,
	"ACCOUNT_NOT_FOUND":                 12000,
	"PREVIOUS_USER_NOT_SIGNED_OUT":      13000,
}

func (x Code) String() string {
	return proto.EnumName(Code_name, int32(x))
}

func (Code) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_6e9b0151640170c3, []int{0}
}

func init() {
	proto.RegisterEnum("errors.Code", Code_name, Code_value)
}

func init() { proto.RegisterFile("code.proto", fileDescriptor_6e9b0151640170c3) }

var fileDescriptor_6e9b0151640170c3 = []byte{
	// 296 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x64, 0x90, 0x4f, 0x4a, 0x33, 0x41,
	0x10, 0xc5, 0x3f, 0x3e, 0x42, 0x08, 0xb5, 0x90, 0xb6, 0x03, 0x31, 0xa0, 0x2e, 0xa2, 0xe8, 0xc2,
	0x45, 0xb2, 0xf0, 0x04, 0x93, 0x4c, 0x2b, 0x43, 0x62, 0xd5, 0xd8, 0x7f, 0x8c, 0x0a, 0xd2, 0x4c,
	0x66, 0x1a, 0x33, 0x10, 0x69, 0x19, 0x47, 0x4f, 0xe0, 0x01, 0x5c, 0xba, 0xf4, 0x08, 0x1e, 0xc0,
	0x03, 0x78, 0x84, 0x2c, 0x3d, 0x84, 0x0b, 0x97, 0xc2, 0x98, 0x28, 0xe8, 0xfa, 0x51, 0xbf, 0x5f,
	0xbd, 0x07, 0x90, 0xfa, 0xcc, 0x75, 0xaf, 0x0b, 0x5f, 0x7a, 0x5e, 0x77, 0x45, 0xe1, 0x8b, 0x9b,
	0xbd, 0xc7, 0xff, 0x50, 0x1b, 0xf8, 0xcc, 0xf1, 0x06, 0xd4, 0x90, 0x50, 0xb0, 0x7f, 0x7c, 0x17,
	0x3a, 0xc1, 0x60, 0x40, 0x06, 0xb5, 0x45, 0xd2, 0xf6, 0x80, 0x0c, 0x86, 0x96, 0xa4, 0xed, 0x07,
	0xa1, 0x8d, 0x03, 0xa5, 0xc6, 0x24, 0x43, 0xf6, 0x80, 0x7c, 0x0d, 0xb8, 0x51, 0x42, 0x5a, 0x24,
	0x1b, 0x0b, 0x79, 0x14, 0x29, 0x15, 0x11, 0xb2, 0xf7, 0x9f, 0xc0, 0xe0, 0x10, 0x69, 0x8c, 0x56,
	0xd3, 0x50, 0x20, 0x7b, 0x89, 0x79, 0x1b, 0x9a, 0x55, 0x10, 0x8c, 0xa4, 0x08, 0xc2, 0x33, 0x2b,
	0x4e, 0x23, 0xa5, 0x15, 0x7b, 0x3a, 0xe6, 0x9b, 0xd0, 0x5e, 0x3a, 0x25, 0x8d, 0x84, 0xaa, 0xcc,
	0x15, 0x55, 0xb3, 0x7b, 0xc9, 0x9b, 0xb0, 0xb2, 0x50, 0x2d, 0xfe, 0x61, 0xcf, 0x9a, 0xaf, 0x43,
	0x6b, 0x79, 0xf3, 0x0b, 0xf8, 0x61, 0x78, 0x0b, 0x56, 0xff, 0x94, 0x60, 0x6f, 0x17, 0xbc, 0x03,
	0x1b, 0xb1, 0x14, 0x27, 0x11, 0x19, 0x65, 0xbf, 0x91, 0x2a, 0x3a, 0x44, 0x11, 0x5a, 0x32, 0x9a,
	0xbd, 0xba, 0xad, 0x7a, 0x63, 0x4e, 0x6c, 0x4e, 0xfd, 0x9d, 0xf3, 0xed, 0xcb, 0xbc, 0x9c, 0x25,
	0x93, 0x6e, 0xea, 0xaf, 0x7a, 0x33, 0x97, 0xdd, 0xa6, 0xe5, 0x34, 0xc9, 0x7b, 0x49, 0x91, 0x4e,
	0xf3, 0x3b, 0xd7, 0xfb, 0x5a, 0x70, 0x52, 0xaf, 0x06, 0xdd, 0xff, 0x0c, 0x00, 0x00, 0xff, 0xff,
	0xeb, 0x31, 0xb1, 0x31, 0x5e, 0x01, 0x00, 0x00,
}
