package dm

import (
	archive "gitlab.com/leducthai/archive"
	"gitlab.com/leducthai/archive/impl/services/auth"
	"gitlab.com/leducthai/archive/impl/services/profile"
)

func (dm *DataManager) Profile() archive.ProfileServices {
	return profile.NewService(dm.db)
}

func (dm *DataManager) Authentication() archive.AuthenticationService {
	return auth.NewService(dm.db)
}
