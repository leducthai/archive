package auth

import (
	"bytes"
	"context"
	"fmt"
	"time"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"

	"gitlab.com/leducthai/archive"
	"gitlab.com/leducthai/archive/impl/orm/models"
	"gitlab.com/leducthai/archive/utils/roles"
)

type Service struct {
	db *gorm.DB
}

func NewService(db *gorm.DB) archive.AuthenticationService {
	return &Service{db: db}
}

func (s *Service) checkAccount(username string, password string) ([]roles.Role, error) {
	var account models.Account
	if err := s.db.Where(models.Account{ID: username}).Take(&account).Error; err != nil {
		return nil, err
	}

	if !bytes.Equal(account.Password, models.Encrypt([]byte(password))) {
		return nil, fmt.Errorf("wrong password")
	}

	rs := make([]roles.Role, len(account.Roles))
	for idx, r := range account.Roles {
		rs[idx] = roles.Role(r)
	}

	return rs, nil
}

// Login implement.
func (s *Service) Login(ctx context.Context, req archive.LoginRequest) (archive.LoginReply, error) {
	// check account.
	roles, err := s.checkAccount(req.Username, req.Password)
	if err != nil {
		return archive.LoginReply{}, err
	}

	// create token.
	expiryTime := time.Now().Add(time.Hour * 8).In(time.Local)
	token, err := s.createToken(req.Username, expiryTime, roles)
	if err != nil {
		return archive.LoginReply{}, err
	}

	return archive.LoginReply{
		Token:           token,
		TokenExpiryTime: expiryTime,
		Role:            roles,
	}, nil
}

func (s *Service) createToken(user string, expiryTime time.Time, info []roles.Role) (string, error) {
	token, err := uuid.NewV4()
	if err != nil {
		return "", err
	}
	// insert token.
	if err := s.db.Create(&models.Token{
		ID:          models.EncryptedData(token.String()),
		BoundUser:   user,
		ExpiryTime:  expiryTime.UnixNano(),
		CreatedTime: time.Now().UnixNano(),
		Valid:       true,
		Info:        info,
	}).Error; err != nil {
		return "", err
	}

	return token.String(), nil
}
