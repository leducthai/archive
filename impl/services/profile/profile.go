package profile

import (
	"context"

	"gorm.io/gorm"

	archive "gitlab.com/leducthai/archive"
)

type service struct {
	db *gorm.DB
}

func NewService(db *gorm.DB) archive.ProfileServices {
	return service{db: db}
}

// NOTE: not fully implemented
func (s service) GetProfile(ctx context.Context, req archive.GetProfileRequest) (archive.GetProfileReply, error) {
	return archive.GetProfileReply{}, nil
}
