package models

import (
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

// Model definition.
type Model interface {
	schema.Tabler
}

// Function definition.
type Function interface {
	MigrateFunction(db *gorm.DB) error
}

// Trigger definition.
type Trigger interface {
	MigrateTrigger(db *gorm.DB) error
}

func GetModelList() []Model {
	return []Model{
		&Profile{},
		&User{},
	}
}
