package models

import (
	"github.com/lib/pq"

	"gitlab.com/leducthai/archive/utils/roles"
)

type User struct {
	ID       string        `gorm:"type:text;primaryKey"`
	Email    Email         `gorm:"type:text;unique"`
	Password EncryptedData `gorm:"type:bytea;not null"`
	Profile  string        `gorm:"type:text;not null"`
}

func (*User) TableName() string {
	return "user"
}

// Account defines who can sign in.
type Account struct {
	// ID is relative to User.ID.
	ID       string        `gorm:"type:text;primaryKey"`
	Password EncryptedData `gorm:"type:bytea;not null"`
	Roles    pq.Int64Array `gorm:"type:smallint[];default:'{}';not null"`
}

// TableName implements.
func (Account) TableName() string {
	return "account"
}

// Token table definition.
type Token struct {
	ID EncryptedData `gorm:"type:bytea;primaryKey"`

	// BoundUser means the token is bound to the user.
	BoundUser string `gorm:"type:text;not null"`

	// ExpiryTime is expiry time of the token. The number of nanoseconds
	// elapsed since January 1, 1970 UTC.
	ExpiryTime int64 `gorm:"not null"`

	// CreatedTime is the time that the token is created. The number of
	// nanoseconds elapsed since January 1, 1970 UTC.
	CreatedTime int64 `gorm:"not null;autoCreateTime:nano"`

	// Valid is whether the token is valid or not.
	Valid bool `gorm:"default:true;not null"`

	Info []roles.Role `gorm:"type:json;default:'{}';not null"`
}
