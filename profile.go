package archive

// GetProfileReply definition for profile reply
type GetProfileReply struct {
	Name string
	Bio  string
	Sex  string
}

// GetProfileRequest definitio for profile request
type GetProfileRequest struct {
	ID string
}
