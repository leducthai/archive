// Code generated by protoc-gen-go. DO NOT EDIT.
// source: roles.proto

package roles

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Role int32

const (
	// Administrator
	Role_ADMINISTRATOR Role = 0
	// special user
	Role_SPECIAL_USER Role = 1
	// normal user
	Role_NORMAL_USER Role = 2
)

var Role_name = map[int32]string{
	0: "ADMINISTRATOR",
	1: "SPECIAL_USER",
	2: "NORMAL_USER",
}

var Role_value = map[string]int32{
	"ADMINISTRATOR": 0,
	"SPECIAL_USER":  1,
	"NORMAL_USER":   2,
}

func (x Role) String() string {
	return proto.EnumName(Role_name, int32(x))
}

func (Role) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_b96358c61fe6d5ae, []int{0}
}

func init() {
	proto.RegisterEnum("roles.Role", Role_name, Role_value)
}

func init() { proto.RegisterFile("roles.proto", fileDescriptor_b96358c61fe6d5ae) }

var fileDescriptor_b96358c61fe6d5ae = []byte{
	// 102 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x2e, 0xca, 0xcf, 0x49,
	0x2d, 0xd6, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0x05, 0x73, 0xb4, 0x6c, 0xb8, 0x58, 0x82,
	0xf2, 0x73, 0x52, 0x85, 0x04, 0xb9, 0x78, 0x1d, 0x5d, 0x7c, 0x3d, 0xfd, 0x3c, 0x83, 0x43, 0x82,
	0x1c, 0x43, 0xfc, 0x83, 0x04, 0x18, 0x84, 0x04, 0xb8, 0x78, 0x82, 0x03, 0x5c, 0x9d, 0x3d, 0x1d,
	0x7d, 0xe2, 0x43, 0x83, 0x5d, 0x83, 0x04, 0x18, 0x85, 0xf8, 0xb9, 0xb8, 0xfd, 0xfc, 0x83, 0x7c,
	0x61, 0x02, 0x4c, 0x49, 0x6c, 0x60, 0xb3, 0x8c, 0x01, 0x01, 0x00, 0x00, 0xff, 0xff, 0x54, 0xa7,
	0x95, 0x4e, 0x5a, 0x00, 0x00, 0x00,
}
